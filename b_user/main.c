#include "board.h"
#include "rtthread.h"
#include "rtdef.h"

#include "msgqueue_demo.h"
#include "semaphore_demo.h"
#include "mutex_demo.h"



#define THREAD_STACKSIZE		512
#define RECV_THREAD_PRIO		4
#define RECV_THREAD_TIMESLICE	30



static rt_thread_t led0_blink_thread_t = RT_NULL;
 void led0_blink_entry(void *p_arg)
{
	int i = 2;
	do{
		GPIO_ResetBits(GPIOB,GPIO_Pin_5);	//led0_on
		rt_thread_delay(1000);
		GPIO_SetBits(GPIOB,GPIO_Pin_5);		//led0_off
		rt_thread_delay(1000);
		rt_kprintf("led0_blink_entry;  \r\n");
	}while(i--);
}




int main(void)
{
	led0_blink_thread_t = rt_thread_create("led0_blink",
											led0_blink_entry,
											RT_NULL,
											THREAD_STACKSIZE,
											RECV_THREAD_PRIO,
											RECV_THREAD_TIMESLICE);
	if(led0_blink_thread_t)
		rt_thread_startup(led0_blink_thread_t);
	else
		return -1;

	//msgqueue_demo();
	//semaphore_sample();
	mutex_sample();



}











