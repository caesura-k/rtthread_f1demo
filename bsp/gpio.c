#include"gpio.h"
//用GPIO主要就是配置完了CRL和CRH之后，就可以通过函数读写操作了；
//CRL和CRH：通过GPIO_Init()来配置；
//IDR和ODR：那几个GPIO的读写函数；
//BSRR和BRR：那几个GPIO的位操作函数；
//KEY0:PE4,   KEY1:PE3,   KEY2:PE2;

void led0_light(void)
{
	gpio_pe4_init();
	gpio_pb5_init();
	int ret = 1 ;
	while(1)
	{
		ret = GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_4);
		if(!ret)
			delay_ms(5);
		if(!ret)
		{
			for(ret=0;ret<3;ret++){
				GPIO_ResetBits(GPIOB,GPIO_Pin_5);	//led0_on
				delay_ms(500);
				GPIO_SetBits(GPIOB,GPIO_Pin_5);		//led0_off
				delay_ms(500);
			}

		}	
	}
}

//KEY0 PE4
void gpio_pe4_init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
	GPIO_InitTypeDef GPIO_Struct;

	GPIO_Struct.GPIO_Pin = GPIO_Pin_4;			
	GPIO_Struct.GPIO_Mode =GPIO_Mode_IPU;	
	GPIO_Init(GPIOE, &GPIO_Struct); 	
}

//LED0 PB5
void gpio_pb5_init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_InitTypeDef GPIO_Struct;
					
	GPIO_Struct.GPIO_Pin = GPIO_Pin_5; 
	GPIO_Struct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Struct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB, &GPIO_Struct);
}


//msg_queue key1 key2 test;
void gpio_key1_key2_init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
	GPIO_InitTypeDef GPIO_Struct;

	GPIO_Struct.GPIO_Pin = GPIO_Pin_3|GPIO_Pin_2;			
	GPIO_Struct.GPIO_Mode =GPIO_Mode_IPU;	
	GPIO_Init(GPIOE, &GPIO_Struct); 	
}



