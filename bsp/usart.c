#include "usart.h"  


u8 USART1_REV_BUF[256];	
u8 USART1_REV_CNT = 0;	
u8 USART1_REV_FLAG = 0;	
/***收到\r\n作为回发数据的标志位，所以没收到\r\n是不会回发的哦；
***需要注意一下串口助手通常默认勾选了“发送新行”，也就是“\r\n”；***/


u8 USART2_REV_BUF[256];
u8 USART2_REV_CNT = 0;
u8 USART2_REV_FLAG = 0; 


void usart1_init(u32 bound)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_USART1, ENABLE);
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);    

    //USART1外设中断配置
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3 ;	
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;			
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;				
    NVIC_Init(&NVIC_InitStructure);  
	
    //GPIO初始化 USART1_TX	PA9
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; 
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;		
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    //GPIO初始化    USART1_RX	PA10
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

   //USART1初始化
    USART_InitStructure.USART_BaudRate = bound;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;   //CR1中的TE,RE  
    USART_Init(USART1, &USART_InitStructure); 
    
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//CR1中的RXNEIE中断
    USART_Cmd(USART1, ENABLE);                    //CR1中的UE使能
}

#if 0
void USART1_sendbuff(u8 *buf,u16 len)
{
    u16 t;
    for(t=0;t<len;t++)        
    {
        USART_SendData(USART1,buf[t]);
		while(USART_GetFlagStatus(USART1,USART_FLAG_TC)==RESET); 
		//等待TC硬件置1，则发送完毕，继续发送；
    }	
}
#else
void USART1_sendbuff(u8 *buf,u16 len)
{
	u16 t=0;
	for(t=0;t<len;t++)
	{
		USART_SendData(USART1,buf[t]);
		while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET)
			;//waiting hard TXE set, till TDR is available;
	}
}
#endif


void USART1_IRQHandler(void)                    
{
    if(USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
	{
     	USART1_REV_BUF[USART1_REV_CNT] =USART_ReceiveData(USART1);
        USART1_REV_CNT++; 
		
        if( (USART1_REV_BUF[USART1_REV_CNT-2]==0x0d) && (USART1_REV_BUF[USART1_REV_CNT-1]==0x0a) )
		{
			USART1_REV_FLAG = 1;
        	printf(" IRQ USART1_REV_FLAG    \r\n");
		}
	}
	
	//详见<中文参考手册>25.3.3 字符接收小节
    if(USART_GetFlagStatus(USART1,USART_FLAG_ORE) == SET)
    {
    	printf("USART_FLAG_ORE  \r\n");
        USART_ReceiveData(USART1);
        //USART_ClearFlag(USART1,USART_FLAG_ORE);
        //先读SR,后读DR，可以复位ORE位；但是前面有个bug可能是卡在这里了，就是离谱，注释掉等下次出现再看看；
        
    }
}

/***等待上位机发送\r\n后，USART1_REV_FLAG置1，然后回发数据；***/
void usart1_receive_test(int bound)
{
    usart1_init(bound); 
	while(1)
	{
		if(USART1_REV_FLAG)
		{
			USART1_REV_FLAG = 0;
			
			printf( "USART1_REV_FLAG 1，return data: \r\n");
			USART1_sendbuff(USART1_REV_BUF,USART1_REV_CNT);
			
			USART1_REV_CNT = 0;
			
			
	   }
	}

}


void usart2_init(u32 bound)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;		
    GPIO_Init(GPIOA, &GPIO_InitStructure);	//PA2 USART2_TX

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;	
    GPIO_Init(GPIOA, &GPIO_InitStructure);	//PA3 USART2_RX

    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);		
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;			
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;	
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;			
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;				
    NVIC_Init(&NVIC_InitStructure);

    USART_InitStructure.USART_BaudRate = bound;							
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;			
    USART_InitStructure.USART_StopBits = USART_StopBits_1;				
    USART_InitStructure.USART_Parity = USART_Parity_No;					
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;		
    USART_Init(USART2, &USART_InitStructure);			
    
    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);		
    USART_Cmd(USART2, ENABLE);                    

}

void USART2_sendbuff(u8 *buf,u16 len)
{
    u16 t;
      for(t=0;t<len;t++)        
    {        
        USART_SendData(USART2,buf[t]);
		while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
    }
}

void USART2_IRQHandler(void)
{
     if(USART_GetITStatus(USART2, USART_IT_RXNE) == SET) 
    {
    	USART2_REV_BUF[USART2_REV_CNT] =USART_ReceiveData(USART2);     
		USART2_REV_CNT++;
		if(USART2_REV_BUF[USART2_REV_CNT-2]==0x0d&&USART2_REV_BUF[USART2_REV_CNT-1]==0x0a)
			USART2_REV_FLAG=1;                        
    }
}





#if 1
/***
***半主机模式就是半独立的主机模式，是单片机的一种功能；为什么是半独立的呢？
***因为它没有自己配套的输入输出设备，需要和其他主机通信，借用其他主机配套的输入输出设备显示；
***那该怎么使用半主机模式呢？通过仿真器连接其他主机，代码中应该就可以直接使用printf和scanf了；
***
***printf和scanf属于"stdio.h"库函数；stdio.h库属于microLIB库；
***#pragma import(__use_no_semihosting)禁用半主机模式，你都禁用了还怎么调用fputc和fgetc呢？
***
***/
/***使用举例：
	printf("enter byte:\r\n");
	scanf("%2x",&scan_num);
	printf("%2x",scan_num);***/

//#pragma import(__use_no_semihosting)   
//上面这个定义和microLIB冲突，所以“勾选microLIB、#pregma”二选一；我选勾microLIB;
//Error: L6915E: Library reports error: __use_no_semihosting was requested, but a semihosting fgetc was linked in

//下面这些函数都是不使用mircoLIB时需要配置的函数，我使用microLIB注释掉好像也没什么问题；               
//struct __FILE 
//{ 
//	int handle; 
//}; 
//FILE __stdout; 
//FILE __stdint;

//调用了库函数之后都会通过_sys_exit()退出库；  
//void _sys_exit(int x) 
//{ 
//	x = x; 
//} 

int fputc(int ch, FILE *f)
{
	while( !(USART1->SR&USART_FLAG_TXE) )
		;
		/*等待TC置1，发送完毕*/  
    USART1->DR = (u8) ch;      
	return ch;
}

//编译没有问题，等待扫描的样子也有，但是数据不对，这个get函数跑不对有点离谱，先放着吧；
//所以这个代码scanf不能用，printf能用；
int fgetc(FILE *f)
{
	while( !(USART1->SR&USART_FLAG_RXNE) )
		;
	return (u8)USART1->DR;
}

#endif 






