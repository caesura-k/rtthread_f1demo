#include "delay.h"





#if USE_STDLIB_SUPPORT_FUNCTION
/***AHB_72MHz，systick_8分频，时钟周期为1/9us，函数计数范围：(1/9)*0xFF FFFF=1.864s;*************/
int delay_ms(int ms)
{
	unsigned int load_count =0;
	unsigned int load_done_flag = 0;
	
	load_count = SystemCoreClock/(1000*8)*ms;
	if (load_count > SysTick_LOAD_RELOAD_Msk)  
		return 1;

	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);
	
	SysTick->LOAD  = (load_count & SysTick_LOAD_RELOAD_Msk) - 1; 	 
	SysTick->VAL   = 0; 										 
	SysTick->CTRL  = SysTick_CTRL_TICKINT_Msk	| 
					 SysTick_CTRL_ENABLE_Msk;	  
	do{
		load_done_flag = SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk;
		}while(!load_done_flag);
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;


	return 0;
}




/***AHB_72MHz，时钟周期为1/72us，函数计数范围：(1/72)*0xFF FFFF=0.233s;**********/
int delay_us(int us)
{
	unsigned int load_count =0;
	unsigned int load_done_flag = 0;
	
	load_count = SystemCoreClock/(1000*1000)*us;

	SysTick_Config(load_count);
	do{
		load_done_flag = SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk;
		}while(!load_done_flag);
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
	

	return 0;
}





#else
/*
*0xFFFFFF=16777215次;AHB为72M，systick为8分频，每次振动为1/9000000s,则systick定时范围约1.86秒；
*1 设置systick的时钟为AHB/8；
*2 load_ms_count 每毫秒内systick时钟源震动的次数；
*3 定时范围超过计时范围return1;定时范围约1.86秒；
*/
int delay_ms(int ms)
{
	u32  load_ms_count = 0;
	u32  load_total = 0;
	u32 temp_systick = 0;


	load_ms_count = SystemCoreClock/(8*1000);					 
	load_total = ms*load_ms_count;
	if(load_total > 0xFFFFFF) return 1;
	
	SysTick->LOAD = load_total;
	SysTick->VAL = 0;
	SysTick->CTRL &= SysTick_CLKSource_HCLK_Div8;
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk | 
					 SysTick_CTRL_TICKINT_Msk ;
	do{
		temp_systick = SysTick->CTRL;
	}while(!(temp_systick&SysTick_CTRL_COUNTFLAG_Msk));
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
	SysTick->LOAD = 0;
	SysTick->VAL = 0;
	return 0;
}



/*计时范围不超过0.23s；*/
int delay_us(int us)
{
	u32  load_us = 0;
	u32  load_total = 0;
	u32 temp_systick = 0;


	load_us = SystemCoreClock/(1000*1000);					 
	load_total = us*load_us;
	if(load_total > 0xFFFFFF) return 1;
	
	SysTick->LOAD = load_total;
	SysTick->VAL = 0;
	SysTick->CTRL = SysTick_CTRL_ENABLE_Msk | 
					SysTick_CTRL_TICKINT_Msk |
					SysTick_CTRL_CLKSOURCE_Msk;
	do{
		temp_systick = SysTick->CTRL;
	}while(!(temp_systick&SysTick_CTRL_COUNTFLAG_Msk));
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
	SysTick->LOAD = 0;
	SysTick->VAL = 0;
	return 0;
}




#endif
/*************************************************************************************************/









