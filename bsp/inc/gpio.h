#ifndef GPIO_H_
#define GPIO_H_
//KEY0:PE4,   KEY1:PE3,   KEY2:PE2;

#include "stm32f10x.h"
#include "delay.h"

void led0_light(void);
void gpio_pe4_init(void);
void gpio_pb5_init(void);
void gpio_key1_key2_init(void);




#endif
