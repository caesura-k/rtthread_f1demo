#ifndef DELAY_H_
#define DELAY_H_



/************************************************************************************************
***标准库提供了两个systick使用函数，SysTick_CLKSourceConfig()，SysTick_Config()以及中断处理函数
***两个的功能其实冲突了，反正都免不了读取CTRL寄存器判断标志位，那还不如直接操作寄存器方便；
****1__使用标准库提供的库函数，
****0__还是直接对寄存器操作;      
**************************************************************************************************/
#define USE_STDLIB_SUPPORT_FUNCTION		1

#include "stm32f10x.h"	



int delay_ms(int ms);
int delay_us(int us);


#endif

