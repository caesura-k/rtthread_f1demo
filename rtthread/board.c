#include <rthw.h>
#include <rtthread.h>
#include "board.h" 
#include "gpio.h"
#include "usart.h"



#if defined(RT_USING_USER_MAIN) && defined(RT_USING_HEAP)
#define RT_HEAP_SIZE 1024
static uint32_t rt_heap[RT_HEAP_SIZE];	// heap default size: 4K(1024 * 4)
RT_WEAK void *rt_heap_begin_get(void)
	{
    return rt_heap;
}

RT_WEAK void *rt_heap_end_get(void)
{
    return rt_heap + RT_HEAP_SIZE;
}
#endif



//初始化systick和板子硬件;
void rt_hw_board_init()
{

	//systick初始化;传入tick为每秒时钟频率千分之一，即systick为1ms;
	SysTick_Config(SystemCoreClock / RT_TICK_PER_SECOND);

	//led0和key0初始化;
	gpio_pb5_init();
	gpio_key1_key2_init();
	
	usart1_init(115200);

	

    /* Call components board initial (use INIT_BOARD_EXPORT()) */
#ifdef RT_USING_COMPONENTS_INIT
    rt_components_board_init();
#endif
    
#if defined(RT_USING_CONSOLE) && defined(RT_USING_DEVICE)
	rt_console_set_device(RT_CONSOLE_DEVICE_NAME);
#endif
    
#if defined(RT_USING_USER_MAIN) && defined(RT_USING_HEAP)
    rt_system_heap_init(rt_heap_begin_get(), rt_heap_end_get());
#endif
}



void SysTick_Handler(void)
{
	rt_interrupt_enter();
	rt_tick_increase();
	rt_interrupt_leave();
}



//使用usart1作为rt_kprintf() output,函数弱声明在kservice.c中;与单片机串口1的重定向并不冲突;
//这个函数是可以被打断的，通过静态变量rt_scheduler_lock_nest来统计嵌套的次数;
void rt_hw_console_output(const char *str)
{
	rt_enter_critical();
	//"..."字符串的数据末尾会自动添加 "\0" ,所以可以通过"\0"来判断字符串是否结尾;
    while (*str!='\0'){
        if (*str=='\n'){
			USART_SendData(USART1, '\r'); 
			while (!USART_GetFlagStatus(USART1, USART_FLAG_TXE))
				;
		}

		USART_SendData(USART1, *str++); 				
		while (!USART_GetFlagStatus(USART1, USART_FLAG_TXE))
			;	
	}	
	rt_exit_critical();
}




