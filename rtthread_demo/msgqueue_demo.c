#include "rtthread.h"
#include "rtdef.h"
#include "gpio.h"





static rt_thread_t receive_thread = RT_NULL;	//消息队列 接收线程结构体指针
static rt_thread_t send_thread = RT_NULL;		//消息队列 发送线程结构体指针
static rt_mq_t test_mq = RT_NULL;				//消息队列 结构体指针
static void msgqueue_receive_entry(void* parameter);
static void msgqueue_send_entry(void* parameter);



//始终循坏遍历，当消息队列接收到数据的时候就读取至数组内;
 static void msgqueue_receive_entry(void* parameter)
{	 
	rt_err_t uwRet = RT_EOK;  
	uint8_t r_queue[10] ={0};
	int i = 0 ;

	while (1)
	{
		uwRet = rt_mq_recv(test_mq, 
							&r_queue,			
							sizeof(r_queue),		
							RT_WAITING_FOREVER);	 
		if(RT_EOK == uwRet)
		{
			rt_kprintf("接收数据是：");
			for(i=0;i<sizeof(r_queue);i++)
				rt_kprintf("%d",r_queue[i]);
			rt_kprintf("\n");
		}
		else
		{
			rt_kprintf("数据接收出错,错误代码: 0x%lx\n",uwRet);
		}
		rt_thread_delay(200);
	}
}


 
//按下key1键，则发送数据至消息队列; 
 static void msgqueue_send_entry(void* parameter)
{	 
	rt_err_t uwRet = RT_EOK;  
	uint8_t send_data1[10] ={1,2,3,4,5,6,7,8,9,10};
	uint32_t n_key = 1 ;
	while (1)
	{
		n_key = GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_3);
		if( !n_key )
		{
			uwRet = rt_mq_send( test_mq,	
								&send_data1,			
								sizeof(send_data1));			 
			if(RT_EOK != uwRet)
				rt_kprintf("数据不能发送到消息队列！错误代码: %lx\n",uwRet);
		} 
		rt_thread_delay(200);
	}
}



//在main函数中包含头文件，然后调用msgqueue_demo();即可;
int msgqueue_demo(void)
{
	 
	 /* 创建一个消息队列 */
	 test_mq = rt_mq_create("test_mq",		 /* 消息队列名字 */
						40, 				 /* 消息的最大长度 */
						20, 				 /* 消息队列的最大容量 */
						RT_IPC_FLAG_FIFO);	 /* 队列模式 FIFO(0x00)*/
	 if (test_mq )
		 rt_kprintf("消息队列创建成功！\n\n");
	 
	 
	 /* 线程控制块指针 */  
	 receive_thread = rt_thread_create( "receive",			 
									 msgqueue_receive_entry, 
									 RT_NULL,				 
									 512,					 
									 3, 					 
									 20);					 
	 if (receive_thread)
		 rt_thread_startup(receive_thread);
	 else
		 return -1;
	 
	   
	   
	 send_thread = rt_thread_create( "send",				 
									 msgqueue_send_entry,
									 RT_NULL,				 
									 512,					 
									 2, 					 
									 20);					 
	 if (send_thread)
		 rt_thread_startup(send_thread);
	 else
		 return -1;  

	 return 0;

}


