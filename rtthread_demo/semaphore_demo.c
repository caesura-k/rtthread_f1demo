#include <rtthread.h>
#include "stm32f10x.h"



#define THREAD_STACKSIZE		512
#define RECV_THREAD_PRIO		3
#define RECV_THREAD_TIMESLICE	30
#define SEND_THREAD_PRIO		2
#define SEND_THREAD_TIMESLICE	30


static rt_thread_t receive_thread = RT_NULL;
static rt_thread_t send_thread = RT_NULL;
static rt_sem_t test_sem = RT_NULL;


static void receive_thread_entry(void* parameter);
static void send_thread_entry(void* parameter);



int semaphore_sample(void)
{
	//创建信号量
	test_sem = rt_sem_create("test_sem", 
							5, 
							RT_IPC_FLAG_FIFO);
	if(test_sem)
		rt_kprintf("计数信号量创建成功! \n");


	
	//创建接收线程
	receive_thread = rt_thread_create("receive",
									receive_thread_entry,
									RT_NULL,
									THREAD_STACKSIZE,
									RECV_THREAD_PRIO,
									RECV_THREAD_TIMESLICE);
	if(receive_thread)
		rt_thread_startup(receive_thread);
	else
		return -1;



	//创建发送线程
	send_thread = rt_thread_create("send",
									send_thread_entry,
									RT_NULL,
									THREAD_STACKSIZE,
									SEND_THREAD_PRIO,
									SEND_THREAD_TIMESLICE);
	if(send_thread)
		rt_thread_startup(send_thread);
	else
		return -1;
	
	return 0;


}





static void receive_thread_entry(void* parameter)
{
	rt_err_t uwRet = RT_EOK;
	uint32_t n_key = 1 ;
	while(1){	
		n_key = GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_3);
		if( !n_key ){
			
			uwRet = rt_sem_take(test_sem, 0 );
			
			if(uwRet == RT_EOK)
				rt_kprintf( "KEY1被单击：成功申请到停车位。\n" );
			else
				rt_kprintf( "KEY1被单击：不好意思，现在停车场已满！\n" );
		}
		rt_thread_delay(200);
	}

}



static void send_thread_entry(void* parameter)
{
	rt_err_t uwRet = RT_EOK;
	uint32_t n_key = 1 ;
	while(1){		
		n_key = GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_2);
		if( !n_key ){
			
			uwRet = rt_sem_release(test_sem);
			
			if(uwRet == RT_EOK)
				rt_kprintf( "KEY2被单击：释放1个停车位。\n" );
			else
				rt_kprintf( "KEY2被单击：但已无车位可以释放！\n" );
		}
		rt_thread_delay(200);
	}

}







