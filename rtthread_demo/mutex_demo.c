#include <rtthread.h>
#include "stm32f10x.h"



#define THREAD_STACKSIZE		512
#define RECV_THREAD_PRIO		3
#define RECV_THREAD_TIMESLICE	30
#define SEND_THREAD_PRIO		2
#define SEND_THREAD_TIMESLICE	30



static rt_thread_t receive_thread = RT_NULL;
static rt_thread_t send_thread = RT_NULL;
static rt_mutex_t test_mutex = RT_NULL;



//这两个线程叫这个名字和接收发送没关系，只是沿用了前面ipc函数的框架叫这个名字;
static void receive_thread_entry(void* parameter);
static void send_thread_entry(void* parameter);



int mutex_sample(void)
{
	//创建互斥量
	test_mutex = rt_mutex_create("test_mutex", 
							RT_IPC_FLAG_PRIO);
	if(test_mutex)
		rt_kprintf("互斥量创建成功！ \n");


	
	//创建接收线程
	receive_thread = rt_thread_create("receive",
									receive_thread_entry,
									RT_NULL,
									THREAD_STACKSIZE,
									RECV_THREAD_PRIO,
									RECV_THREAD_TIMESLICE);
	if(receive_thread)
		rt_thread_startup(receive_thread);
	else
		return -1;



	//创建发送线程
	send_thread = rt_thread_create("send",
									send_thread_entry,
									RT_NULL,
									THREAD_STACKSIZE,
									SEND_THREAD_PRIO,
									SEND_THREAD_TIMESLICE);
	if(send_thread)
		rt_thread_startup(send_thread);
	else
		return -1;
	
	return 0;


}




//每500ms申请一次互斥量;申请不到就算了;
static void receive_thread_entry(void* parameter)
{
	rt_err_t return_flag = RT_EOK;
	uint8_t test_flag = 8 ;

	do{	
		return_flag = rt_mutex_take(test_mutex, RT_WAITING_NO );
		
		if(return_flag == RT_EOK){
			rt_kprintf( "receive_entry take mutex \n" );
			rt_thread_delay(500);
			rt_kprintf( "receive_entry release mutex  \n" );
			rt_mutex_release(test_mutex);
			//print放release前是因为release之后会直接从suspend_thread里恢复线程，就会先恢复了下一个线程再回来打印这里;
			
		}
		else{
			rt_kprintf( "receive_entry fail take mutex \n" );
			rt_thread_delay(500);
			
		}	
		
	}while(--test_flag);

}


//每秒申请一次互斥量，要是申请不到互斥量就挂起不执行;
static void send_thread_entry(void* parameter)
{
	uint8_t test_flag = 4 ;

	do{		
		rt_mutex_take(test_mutex,RT_WAITING_FOREVER);
		rt_kprintf( "send_entry take mutex！ \n" );
	
		rt_thread_delay(1000);
		
		rt_kprintf( "send_entry release mutex！ \n" );
		rt_mutex_release(test_mutex);
		
		rt_thread_delay(1000);
	}while(--test_flag);

}







